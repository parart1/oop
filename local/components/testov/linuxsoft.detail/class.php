<?php

use Bitrix\Main\Loader;
use Bitrix\Iblock\Elements\ElementSoftTable;
use Bitrix\Main\Localization\Loc;



if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class SoftDetail extends CBitrixComponent
{
    public function getLinuxSoftElement()
    {
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();


        $softObj = ElementSoftTable::query()
            ->addSelect('ID')
            ->addSelect('NAME')
            ->addSelect('CODE')
            ->addSelect('DETAIL_PICTURE')
            ->addSelect('DETAIL_TEXT')
            ->addSelect('LOGO')
            ->addSelect('LAST_VERSION')
            ->addSelect('AVAILABLE_ON_OS')
            ->setFilter(['CODE' => mb_strtoupper(basename($request->getRequestUri()), 'UTF-8')])
            ->fetchObject();




        $arResult = [
            'NAME' => $softObj->getName(),
            'CODE' => $softObj->getCode(),
            'DETAIL_PICTURE' => CFile::GetPath($softObj->getDetailPicture()),
            'DETAIL_TEXT' => $softObj->getDetailText(),
            'LOGO' => $softObj->getLogo(),
            'AVAILABLE_ON_OS' => $softObj->getAvailableOnOs()['VALUE']

        ];

        foreach ($softObj->getLastVersion()->getAll() as $value) {
            $arResult['LAST_VERSION'][] = $value->getValue();
        }

//
//
//        $arResult['AVAILABLE_ON_OS'] = $softObj->getAvailableOnOs()['VALUE'];
//
//        $arResult['ELEMENT'] = $res->fetch();
//        $arResult['ELEMENT']['DETAIL_PICTURE'] = CFile::GetPath($arResult['ELEMENT']['DETAIL_PICTURE']);

        return $arResult;
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent(): void
    {
        if (!Loader::includeModule('iblock')) {
            ShowError(Loc::getMessage('LS_ERROR_INCLUDE_IBLOCK'));
            return;
        }

        $this->arResult = $this->getLinuxSoftElement();
        $this->includeComponentTemplate();
    }
}