<?php


if (!empty($arResult)): ?>

    <ul>
            <li>
                <?php if (!empty($arResult['DETAIL_PICTURE'])): ?>
                    <img src="<?=$arResult['DETAIL_PICTURE']?>" alt="<?=$arResult['NAME']?>" width="200" height="200">
                <?php endif; ?>
                <h2><?=$arResult['NAME']?></a></h2>
                <?php if (!empty($arResult['DETAIL_TEXT'])): ?>
                    <p><?=$arResult['DETAIL_TEXT']?></p>
                <?php endif; ?>
                <?php if (!empty($arResult['LAST_VERSION'])): ?>
                    <p>Последние версии:
                    <?php foreach ($arResult['LAST_VERSION'] as $version): ?>
                    <?=$version?>
                    <?php endforeach;?>
                    </p>
                <?php endif; ?>
                <?php if (!empty($arResult['AVAILABLE_ON_OS'])): ?>
                    <p>Доступно на ОС: <?=$arResult['AVAILABLE_ON_OS']?></p>
                <?php endif; ?>

            </li>
    </ul>
<?php endif; ?>
