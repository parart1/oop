<?php


use Bitrix\Main\Loader;
use Bitrix\Iblock\Elements\ElementSoftTable;
use Bitrix\Main\Localization\Loc;


if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class TestComponent extends CBitrixComponent
{


    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getLinuxSoftElements(): array
    {
        $arResult = [];
        $res = ElementSoftTable::query()
            ->addSelect('NAME')
            ->addSelect('CODE')
            ->addSelect('PREVIEW_PICTURE')
            ->addSelect('PREVIEW_TEXT')
            ->fetchCollection();

        foreach ($res as $element) {
            if ($element->getPreviewPicture()) {
                $image = CFile::ResizeImageGet($element->getPreviewPicture(), ['width' => 200, 'height' => 200]);

            }
            $arResult['ELEMENTS'][] = [
                'NAME' => $element->getName(),
                'CODE' => $element->getCode(),
                'PREVIEW_PICTURE' => $image['src'],
                'PREVIEW_TEXT' => $element->getPreviewText(),
            ];
        }

        return $arResult;
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent(): void
    {
        if (!Loader::includeModule('iblock')) {
            ShowError(Loc::getMessage('LS_ERROR_INCLUDE_IBLOCK'));
            return;
        }

        $this->arResult = $this->getLinuxSoftElements();
        $this->includeComponentTemplate();
    }
}
