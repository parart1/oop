
<?php
    \Bitrix\Main\Diag\Debug::dump($arResult);
if (!empty($arResult['ELEMENTS'])): ?>

    <ul>
        <?php foreach ($arResult['ELEMENTS'] as $element): ?>
            <li>
                <?php if (!empty($element['PREVIEW_PICTURE'])): ?>
                    <img src="<?=$element['PREVIEW_PICTURE']?>" alt="<?=$element['NAME']?>" width="200" height="200">
                <?php endif; ?>
                <h2><a href="/linux-soft/<?=mb_strtolower($element['CODE'], 'UTF-8')?>/"><?=$element['NAME']?></a></h2>
                <?php if (!empty($element['PREVIEW_TEXT'])): ?>
                    <p><?=$element['PREVIEW_TEXT']?></p>
                <?php endif; ?>

            </li>
        <?php endforeach; ?>
    </ul>

<?php endif; ?>
