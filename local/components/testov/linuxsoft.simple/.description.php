<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


Loc::loadMessages(__FILE__);

$arComponentDescription = [
    'NAME' => Loc::getMessage('LS_SIMPLE_NAME'),
    'DESCRIPTION' => Loc::getMessage('TESTOV_DESCR'),
    'ICON' => '',
    'PATH' => [
        'ID' => 'testov',
        'CHILD' => [
            'ID' => 'ls_simple',
            'NAME' => Loc::getMessage('TESTOV_NAME') ,
        ],
    ],

];