<?php


use Bitrix\Main\Loader;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\Elements\ElementSoftTable;
use Bitrix\Main\Localization\Loc;



if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class TestComponent extends CBitrixComponent
{

    public function getLinuxSoftElements(): array
    {

        $arResult = array();

        $res = ElementSoftTable::getList([

            'select' => ['ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'PREVIEW_TEXT'],
        ]);

        while ($element = $res->fetch()) {
            if ($element['PREVIEW_PICTURE']) {
                $image = CFile::ResizeImageGet($element['PREVIEW_PICTURE'], ['width' => 200, 'height' => 200]);
                $element['PREVIEW_PICTURE'] = $image['src'];

            }
            $arResult['ELEMENTS'][] = $element;
        }


        return $arResult;

    }

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent() : void
    {
        if (!Loader::includeModule('iblock')) {
            ShowError(Loc::getMessage('LS_ERROR_INCLUDE_IBLOCK'));
            return;
        }


        $this->arResult = $this->getLinuxSoftElements();
        $this->includeComponentTemplate();
    }
}
