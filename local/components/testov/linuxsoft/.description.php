<?php

use Bitrix\Main\Localization\Loc;

if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentDescription = [
    'NAME' => Loc::getMessage('LINUX_SOFT_NAME'),
    'DESCRIPTION' => Loc::getMessage('LINUX_SOFT_DESCRIPTION'),
    'SORT' => 10,
    'COMPLEX' => 'Y',
    'PATH' => [
        'ID' => 'testov',
        'NAME' => Loc::getMessage('LINUX_SOFT_GROUP'),
        'SORT' => 10,
        'CHILD' => [
            'ID' => 'linux_soft',
            'NAME' => Loc::getMessage('LINUX_SOFT_CHILD_GROUP'),
            'SORT' => 10,
        ],
    ],
];