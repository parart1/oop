<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = [
    'PARAMETERS' => [
        'VARIABLE_ALIASES' => [
            'SECTION_CODE' => ['NAME' => Loc::getMessage('SOFT_ELEMENTS_GROUP_LIST')],
            'ELEMENT_CODE' => ['NAME' => Loc::getMessage('SOFT_ELEMENTS_GROUP_DETAIL')],
        ],
        'SEF_MODE' => [

            'index' => [
                'NAME' => Loc::getMessage('SOFT_ELEMENTS_SEF_INDEX'),
                'DEFAULT' => '',
                'VARIABLES' => [],
            ],
            'section' => [
                'NAME' => Loc::getMessage('SOFT_ELEMENTS_SEF_SECTION'),
                'DEFAULT' => '#SECTION_CODE#/',
                'VARIABLES' => ['SECTION_CODE'],
            ],
            'detail' => [
                'NAME' => Loc::getMessage('SOFT_ELEMENTS_SEF_DETAIL'),
                'DEFAULT' => '#ELEMENT_CODE#/',
                'VARIABLES' => ['ELEMENT_CODE'],
            ],

        ]
    ]
];