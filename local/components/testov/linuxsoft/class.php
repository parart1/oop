<?php


use Bitrix\Iblock\Component\Tools;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


class LinuxSoftComplex extends CBitrixComponent
{
    protected array $arDefaultUrlTemplatesSEF = [
        'index' => 'index.php',
        'detail' => 'detail.php?ELEMENT_CODE=#ELEMENT_CODE#',
    ];

    protected array $arDefaultUrlTemplates404 = [
        'index' => '',
        'detail' => '#ELEMENT_CODE#/',
    ];

    protected array $arDefaultVariableAliases404 = [
        'news' => [],
        'detail' => [],
    ];

    protected array $arComponentVariables = [
        'SECTION_CODE',
        'ELEMENT_CODE',
    ];

    protected array $arDefaultVariableAliases = [
        "SECTION_CODE" => 'SECTION_CODE',
        "ELEMENT_CODE" => 'ELEMENT_CODE',
    ];

    public function onSPF(): false|string
    {
        $arVariables = [];
        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($this->arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
        $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($this->arDefaultVariableAliases404, $this->arParams['VARIABLE_ALIASES']);

        $componentPage = CComponentEngine::ParseComponentPath(
            $this->arParams['SEF_FOLDER'],
            $arUrlTemplates,
            $arVariables
        );
        $engine = new CComponentEngine($this);
        $componentPage = $engine->guessComponentPath(
            $this->arParams['SEF_FOLDER'],
            $arUrlTemplates,
            $arVariables
        );

        if (!$componentPage)
            $componentPage = 'index';

        CComponentEngine::InitComponentVariables($componentPage, $this->arComponentVariables, $arVariableAliases, $arVariables);
        $this->arResult = [
            "FOLDER" => $this->arParams['SEF_FOLDER'],
            'URL_TEMPLATES' => $arUrlTemplates,
            'VARIABLES' => $arVariables,
            'ALIASES' => $arVariableAliases
        ];

        return $componentPage;
    }


    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent(): void
    {
        if (!Loader::includeModule('iblock')) {
            ShowError(Loc::getMessage('LS_ERROR_INCLUDE_IBLOCK'));
            return;
        }
        if ($this->arParams['SEF_MODE'] == 'Y') {
            $componentPage = $this->onSPF();
        } else {
            $componentPage = $this->getPage();
        }

        $this->IncludeComponentTemplate($componentPage);
    }


    protected function getPage(): string
    {

        $arVariables = [];
        $arDefaultVariableAliases = [];


        $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $this->arParams['VARIABLE_ALIASES']);
        CComponentEngine::initComponentVariables(false, $this->arComponentVariables, $arVariableAliases, $arVariables);
        $this->arResult['CURRENT_ELEMENT_CODE'] = $arVariables['ELEMENT_CODE'];
        if ((isset($arVariables['ELEMENT_CODE']) && $arVariables['ELEMENT_CODE'] <> '')) {
            $componentPage = 'detail';
        } else {
            $componentPage = 'index';
        }
        $this->arResult = [
            'VARIABLES' => $arVariables,
            'ALIASES' => $arVariableAliases
        ];
        return $componentPage;
    }
}