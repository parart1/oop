<?php

namespace Pararti\Entity;

use Bitrix\Main\Entity;

class EventTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'events';
    }
}