<?php

namespace Pararti\Entity;

use Bitrix\Main\Entity;

class RoleTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'roles';
    }
}