<?php

namespace Pararti\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations\Reference;

class UserTable extends Entity\DataManager
{
    public static function getTableName() : string
    {
        return 'users';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
                'unique' => true,
            ]),
            new Entity\StringField('EMAIL', [
                'required' => true,
                'unique' => true,
            ]),
            new Entity\IntegerField('ROLE_ID'),
            new Entity\IntegerField('EVENT_ID'),

            new Reference(
                'ROLE',

            ),
        ];
    }


}
