<?php

namespace Pararti\Roles;

abstract class Base
{
    protected int $id;
    protected string $name;
    protected string $email;

    public function __construct(int $id, string $name, string $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    public function getPersonalData(): string
    {
        return "ID: $this->id Имя: $this->name Email: $this->email \n";
    }

    /**
     * @throws \Exception
     */
    public function getMsgResetPsw(): bool
    {
        return mail(
            $this->email,
            'Восстановление пароля',
            'Ваш новый пароль: ' . bin2hex(random_bytes(8)),
        );
    }


}