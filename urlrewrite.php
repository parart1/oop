<?php
$arUrlRewrite = array(
    4 =>
        array(
            'CONDITION' => '#^/examples/my-components/news/#',
            'RULE' => NULL,
            'ID' => 'demo:news',
            'PATH' => '/examples/my-components/news_sef.php',
            'SORT' => 100,
        ),
    1 =>
        array(
            'CONDITION' => '#^\\/?\\/mobileapp/jn\\/(.*)\\/.*#',
            'RULE' => 'componentName=$1',
            'ID' => NULL,
            'PATH' => '/bitrix/services/mobileapp/jn.php',
            'SORT' => 100,
        ),
    2 =>
        array(
            'CONDITION' => '#^/communication/forum/#',
            'RULE' => '',
            'ID' => 'bitrix:forum',
            'PATH' => '/communication/forum/index.php',
            'SORT' => 100,
        ),
    3 =>
        array(
            'CONDITION' => '#^/communication/blog/#',
            'RULE' => '',
            'ID' => 'bitrix:blog',
            'PATH' => '/communication/blog/index.php',
            'SORT' => 100,
        ),
    0 =>
        array(
            'CONDITION' => '#^/rest/#',
            'RULE' => '',
            'ID' => NULL,
            'PATH' => '/bitrix/services/rest/index.php',
            'SORT' => 100,
        ),
    5 => array(
        'CONDITION' => '#^/linux-soft/([a-z]+)/#',
        'RULE' => 'ELEMENT_CODE=$1',
        'ID' => 'testov:linuxsoft',
        'PATH' => '/linux-soft.php',
    ),
    6 =>
        array(
            'CONDITION' => '#^/linux-soft/#',
            'RULE' => '',
            'ID' => 'testov:linuxsoft',
            'PATH' => '/linux-soft.php',
            'SORT' => 100,
        ),

);
